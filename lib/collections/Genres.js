Genres = new Mongo.Collection('genres');

if (Meteor.isServer) {
    Meteor.publish(null, function() {
        return Genres.find();
    });
}
