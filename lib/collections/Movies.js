Movies = new Mongo.Collection('movies');
if (Meteor.isServer) {
    Meteor.publish('links', function(imdb) {
        check(imdb, String);
        return Movies.find({
            imdb: imdb
        });
    });
}

Movies.before.upsert(function(userId, selector, modifier, options) {
    if (!modifier.mkv.includes('mkvcorporation')) {
        modifier.dls = [modifier.mkv];
        modifier.mkv = null;
    }
});

var manageImdb = function(doc) {
    if (doc.imdb) {
        Meteor.call('updateImdb', doc.imdb);
    } else {
        val = {
            name: doc.name,
            original_title: doc.original_title,
            release_date: doc.release_date
        };
        Meteor.call('findImdb', val);
    }
}

Movies.after.insert(function(userId, doc) {
    manageImdb(doc);
});


Movies.after.update(function(userId, doc, fieldNames, modifier, options) {
    manageImdb(doc);
});
