Imdb = new Mongo.Collection('imdb');
Imdb.after.insert(function(userId, doc) {
    if (doc) {
        doc.genre_ids.forEach(function(genre_id) {
            if (!Genres.findOne({
                    id: genre_id
                })) {
                Meteor.call('addGenre', genre_id);
            }
        });
    }
});
if (Meteor.isServer) {

    Meteor.publish('movies', function(limit, name, sortby, genre_filter) {
        check(limit, Number);
        check(name, Match.Maybe(String));
        check(sortby, Match.Maybe(String));
        check(genre_filter, Match.Maybe(String));

        selectors = [];
        if (name) {
            selectors.push({
                $or: [{
                    'title': {
                        $regex: name,
                        $options: "i"
                    }
                }, {
                    'original_title': {
                        $regex: name,
                        $options: "i"
                    }
                }]

            });
        }

        if (genre_filter && genre_filter != "") {
            selectors.push({
                genre_ids: {
                    $elemMatch: {
                        $eq: parseInt(genre_filter)
                    }
                }
            });
        }

        var sort = {};
        switch (sortby) {
            case 'title':
                sort.title = 1;
                break;
            case 'popularity':
                sort.popularity = -1;
                break
            case 'vote_average':
                sort.vote_average = -1;
                break;
            default:
                sort.title = 1;
        }

        switch (selectors.length) {
            case 0:
                selector = {};
                break;
            case 1:
                selector = selectors[0];
                break;
            default:
                selector = {
                    $and: selectors
                };
                break;
        }
        limit = limit >= Imdb.find(selector).count() ? Imdb.find().count() : limit;
        return Imdb.find(selector, {
            limit: limit,
            sort: sort
        });
    });
}
