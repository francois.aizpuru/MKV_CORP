Template.movieModal.onRendered(function() {
    Meteor.subscribe("links", this.data.imdbid);
});

Template.movieModal.helpers({
    title: function() {
        return this ? this.title : "";
    },
    original_title: function() {
        return this ? this.original_title : "";
    },
    title_different: function() {
        return this.original_title != this.title;
    },
    overview: function() {
        return this ? this.overview : "";
    },
    genres: function() {
        return this ? this.genre_ids : [];
    },
    formatGenre: function(genreid) {
        return Genres.findOne({
            id: genreid
        }).name;
    },
    poster: function() {
        //return "../assets/poster.jpg"; //TO REMOVE
        if (this.poster_path) {
            return " http://image.tmdb.org/t/p/w185" + this.poster_path;
        } else if (this.backdrop_path) {
            return " http://image.tmdb.org/t/p/w300" + this.backdrop_path;
        }
        return "";
    },
    year: function() {
        return this ? this.release_date.split('-')[0] : "";
    },
    dls: function() {
        movie = Movies.findOne({
            imdb: this.imdbid
        });
        if (!movie) return [];
        return Array.isArray(movie.dls) ? movie.dls : [movie.dls];
    },
    short_link: function(link) {
        return link.match(/(?!w{1,}\.)(\w+\.?){1,}([a-zA-Z]+)(\.)/g).toString().replace('.', '').toUpperCase();
    }
});
