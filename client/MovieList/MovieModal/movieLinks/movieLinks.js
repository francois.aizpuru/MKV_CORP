Template.linkRow.events({
    "click .glyphicon-plus": function(event, template) {
        $('a.hidden').each(function() {
            $(this).removeClass('hidden');
        });
        $('.glyphicon-plus').addClass('hidden');
    }
});

Template.linkRow.helpers({
    uptobox: function() {
        var urls = this.urls.filter(function(el) {
            return el.includes('uptobox');
        });
        return urls;
    },
    otherMirrors: function() {
        var urls = this.urls.filter(function(el) {
            return !el.includes('uptobox');
        });
        return urls;
    }
});
