Template.movieBox.helpers({
    name: function() {
        return this.title;
        return "title";
    },
    rating: function() {
        return this.vote_average * 10;
        return 0;
    },
    imdb: function() {
        return this.imdbid;
    },
    poster: function() {
        if (this.poster_path) {
            return " http://image.tmdb.org/t/p/w185" + this.poster_path;
        } else if (this.backdrop_path) {
            return " http://image.tmdb.org/t/p/w300" + this.backdrop_path;
        }
        return "";
    }
});

displayInfoMovie = function(imdb) {
    Modal.show('movieModal', function() {
        return Imdb.findOne({
            imdbid: imdb
        });
    });
}
