var bLazy;

Template.movieList.onRendered(function() {
    Session.set('itemsLimit', 40);
    Session.set('search', "");
    Session.set('genre_filter', "");
    Session.set('sortby', "popularity");


    bLazy = new Blazy();
    Deps.autorun(function() {
        Meteor.subscribe('movies', Session.get('itemsLimit'), Session.get('search'), Session.get('sortby'), Session.get('genre_filter'), function() {
            bLazy = new Blazy();
        });
    });
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
            Session.set('itemsLimit', Session.get('itemsLimit') + 10);
        }
    });
});


Template.movieList.events({
    "input #search": function(event, template) {
        newVal = event.target.value;
        Session.set('search', newVal);
    },
    "change #sortby": function(event, template) {
        newVal = event.target.value;
        Session.set('sortby', newVal);
    },
    "change #filterby": function(event, template) {
        newVal = event.target.value;
        Session.set('genre_filter', newVal);
    }
});
Template.movieList.helpers({
    movies: function() {
        var sort = {};
        switch (Session.get('sortby')) {
            case 'title':
                sort.title = 1;
                break;
            case 'popularity':
                sort.popularity = -1;
                break
            case 'vote_average':
                sort.vote_average = -1;
                break;
            default:
                sort.title = 1;
        }
        return Imdb.find({}, {
            sort: sort
        });
    },
    genres: function() {
        return Genres.find({}, {
            sort: {
                name: 1
            }
        });
    }
});
