var ImdbScheduler = {};

initMdbScheduler = function() {
    ImdbQueue.remove({});
    ImdbScheduler = {
        schedule: function(type, value) {
            if (ImdbQueue.findOne({
                    type: type,
                    value: value
                })) {
                return;
            }
            var action;
            switch (type) {
                case "info":
                    action = updateimdb;
                    break;
                case "genre":
                    action = addGenre;
                    break;
                case "findid":
                    action = findImdb;
                    break;
                default:
                    console.log("type is invalid");
                    return;
            }
            var queue_id = ImdbQueue.insert({
                type: type,
                value: value
            });
            //Limit of 4 requests by second
            timeout = (ImdbQueue.find().count() - 1) * 400;
            setTimeout(Meteor.bindEnvironment(function() {
                action(value);
                ImdbQueue.remove(queue_id);
            }), timeout);
        }
    }
}
