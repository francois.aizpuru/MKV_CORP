var TMDBSearch = {};

initMdb = function() {
    TMDBSearch.api = Meteor.settings.TMDB_KEY;
    TMDBSearch.language = "fr";
    TMDBSearch.request = function(request) {
        try {
            var response = HTTP.call("GET", request, {
                timeout: 15000
            });
        } catch (error) {
            if (error.response.statusCode == 429) {
                console.log("to reschedule");
            } else {
                console.log(error);
            }
            return false;
        }
        return response.data;
    };
    TMDBSearch.getById = function(id) {
        var request = "https://api.themoviedb.org/3/find/" + id +
            "?api_key=" + TMDBSearch.api + "&language=" + TMDBSearch.language +
            "&external_source=imdb_id";
        var data = TMDBSearch.request(request);
        return data;
    };
    TMDBSearch.omdbGetExtId = function(title, year) {
        var request = "http://www.omdbapi.com/?t=" + encodeURI(title) + "&y=" + year;
        var data = TMDBSearch.request(request);
        return data.imdbID;
    };
    TMDBSearch.searchMovie = function(name, year) {
        var request = "https://api.themoviedb.org/3/search/movie/" +
            "?api_key=" + TMDBSearch.api + "&language=" + TMDBSearch.language +
            "&query=" + encodeURI(name) + "&year=" + year;
        var data = TMDBSearch.request(request);
        return data;
    };
    TMDBSearch.getExternalId = function(id) {
        var request = "https://api.themoviedb.org/3/movie/" + id +
            "?api_key=" + TMDBSearch.api + "&language=" + TMDBSearch.language +
            "append_to_response=external_ids";
        var data = TMDBSearch.request(request);
        return data.imdb_id;
    };
    TMDBSearch.getGenreById = function(id) {
        var request = "https://api.themoviedb.org/3/genre/" + id +
            "?api_key=" + TMDBSearch.api + "&language=" + TMDBSearch.language +
            "&external_source=imdb_id";
        var data = TMDBSearch.request(request);
        return data;
    };

}
