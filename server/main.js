import {
    Meteor
} from 'meteor/meteor';

import './ImdbScheduler.js';
import './MovieDatabaseManager.js';

var osmosis;


moviesList = [
    'https://www.mkvcorporation.com/viewtopic.php?id=3459',
    'https://www.mkvcorporation.com/viewtopic.php?id=3460',
    'https://www.mkvcorporation.com/viewtopic.php?id=3461',
    'https://www.mkvcorporation.com/viewtopic.php?id=3462',
];

movieArletty = "https://www.mkvcorporation.com/viewtopic.php?id=9800";
seriesList = ['https://www.mkvcorporation.com/viewtopic.php?id=3464'];


Meteor.startup(() => {
    initOsmosis();
    initMdb();
    initMdbScheduler();
});

var initOsmosis = function() {
    osmosis = require('osmosis');
    var useragent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36';
    var accept = '*/*';
    osmosis.config('user_agent', useragent);
    osmosis.config('accept', accept);
}




Meteor.methods({
    addGenre: function(id) {
        if (!ImdbQueue.findOne({
                type: 'genre',
                id: id
            })) {
            ImdbScheduler.schedule('genre', id);
        }
    },
    findImdb: function(doc) {
        if (!ImdbQueue.findOne({
                type: 'findid',
                id: doc
            })) {
            ImdbScheduler.schedule('findid', doc);
        }
    },
    updateAllMovies: function() {
        mkv_login_params = {
            'form_sent': '1',
            'redirect_url': '',
            'req_username': Meteor.settings.MKV_USER,
            'req_password': Meteor.settings.MKV_PASS,
            'login': 'Identification'
        }
        moviesList.forEach(function(page) {
            mkv_login_url = 'https://www.mkvcorporation.com/login.php?action=in';
            osmosis.post(mkv_login_url, mkv_login_params)
                .done(function() {
                    console.log("Connected to MKV")
                }).get(page)
                .find('strong')
                .set({
                    'mkv': 'a:has(span.postimg):has(span)@href',
                    'name': 'a:has(span.postimg):has(span)'
                })
                .follow('a:has(span.postimg):has(span)@href')
                .set({
                    imdb: 'a[href^="http://www.imdb.com/title/"]@href',
                })
                .find('.mkvtable')
                .set({
                    'dls': osmosis.select('tr').set('dl').set({
                        'urls': ['a@href'],
                        'quality': 'td:nth-child(2)',
                        'codec': 'td:nth-child(3)',
                        'size': 'td:nth-child(4)'
                    })

                })
                .data(Meteor.bindEnvironment(function(result) {
                    if (result.imdb) {
                        result.imdb = result.imdb.match('^http://www.imdb.com/title/(.*)/')[1];
                    } else {
                        year = result.name.match(/\(\d{4}\)/)[0].replace(/[{()}]/g, '');
                        name = result.name.split(/\(/)[0];
                        result.original_title = name;
                        result.release_date = year;
                    }
                    selector = {
                        name: result.name
                    };
                    Movies.upsert(selector, result);
                }));
        });

    },
    updateImdb: function(imdb) {
        ImdbScheduler.schedule('info', imdb);
    },
    updateAllImdb: function() {
        Movies.find({
            "imdb": {
                $exists: true
            }
        }).forEach(function(movie, index) {
            ImdbScheduler.schedule('info', movie.imdb);
        });
    }
});


updateimdb = function(imdb) {
    info = TMDBSearch.getById(imdb);
    //console.log(info.movie_results.lenght);
    if (info && info.movie_results.length > 0) {
        info.movie_results[0].imdbid = imdb;
        Imdb.upsert({
            'imdbid': imdb
        }, info.movie_results[0]);
    } else {
        console.log("---------ERROR: ----------", imdb);
    }
}

addGenre = function(genre_id) {
    genre = TMDBSearch.getGenreById(genre_id);
    if (genre) {
        Genres.upsert({
            id: genre.id
        }, genre);
    }
}

findImdb = function(doc) {
    id = TMDBSearch.omdbGetExtId(doc.original_title, doc.release_date);
    if (!id) {
        mv = TMDBSearch.searchMovie(doc.original_title, doc.release_date).results[0];
        if (mv) {
            id = TMDBSearch.getExternalId(mv.id);
        }
    }
    Movies.update({
        name: doc.name
    }, {
        $set: {
            imdb: id
        }
    });
}
